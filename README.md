<p align="center">
    <img width="280" src="https://raw.githubusercontent.com/zhanyeye/Figure-bed/win-pic/img/20210602141834.png">
</p>
<h1 align="center">Git Flow Test</h1>

> 用于学习测试开发流程

+ `main` 主干分支 （保护分支）
+ `feature` 特性分支 （保护分支)
+ `personal/**`  个人分支

---

测试commit区：
```
commit 1
commit 2
commit 3
commit 4
commit 5
commit 6
commit 7
commit 8
fixbug 1
```
